import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon } from 'native-base';
import {styleApp} from './style';

const styles = styleApp.getStyle();

export default class Home extends React.Component {
  render() {
    return (
      <Container>
        <Header style={styles.bg} >
          <Left>
            <Button onPress={() => this.props.navigation.openDrawer()  }  transparent>
              <Icon name='menu' />
            </Button>
          </Left>
          <Body>
            <Title>Actualite</Title>
          </Body>
          <Right />
        </Header>
        <Content>

        <Button onPress={() => this.props.navigation.navigate("Page2") } success><Text> Page 2 </Text></Button>
        </Content>
        <Footer>
          <FooterTab>
            <Button full style={styles.bg} >
              <Text style={styles.txtColor} >Pied de page</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}
