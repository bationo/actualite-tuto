import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon } from 'native-base';
import {styleApp} from './style';

const styles = styleApp.getStyle();

export default class Page2 extends React.Component {
  render() {
    return (
      <Container>
        <Header style={styles.bg} >
          <Left>
            <Button onPress={() => this.props.navigation.goBack()  }  transparent>
              <Icon name='arrow-back' />
            </Button>
          </Left>
          <Body>
            <Title>Page 2</Title>
          </Body>
          <Right />
        </Header>
        <Content>
          <Text>
            Bienvenue sur page 2
          </Text>
        </Content>
        <Footer>
          <FooterTab>
            <Button full style={styles.bg} >
              <Text style={styles.txtColor} >Pied de page</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}
